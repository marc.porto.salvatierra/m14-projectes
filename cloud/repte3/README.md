# docker
## @edt ASIX-M05 Curs 2021-2022

# Servidor Postgres


En aquesta pràctica es construeix un servidor postgres amb les dades de la base de dades training ja carregades. S’utilitzarà com a imatge base la imatge oficial de postgrees i es copia les dades de training i els scripts d’inicialització al directori d’inicialització de postgres.


Requeriments:

  * Identificar el nom de la imatge postgres oficial.
  * Obtenir les dades de la base de dades training i els scripts d’inicialització.
  * Identificar el directori de postgres que permet automatitzar la inicialització.

```
$ pwd
./docker/dockefiles/postgres21

$ ls
Dockerfile  training
```

```
$ cat Dockerfile 
# Postgres amb base de dades training
FROM library/postgres
ADD . .
COPY training/* /docker-entrypoint-initdb.d/
```

```
## Xixa a la imatge de postgres
docker run --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v $(pwd)/training:/docker-entrypoint-initdb.d --net 2hisx -d postgres
--> Creacio de la base de dades amb el seu contingut

psql -h 172.18.0.2 -U postgres -d training -c "select * from oficinas;"
--> Comprovavió funcionalitat
 oficina |   ciudad    | region | dir | objetivo  |  ventas   
---------+-------------+--------+-----+-----------+-----------
      22 | Denver      | Oeste  | 108 | 300000.00 | 186042.00
      11 | New York    | Este   | 106 | 575000.00 | 692637.00
      12 | Chicago     | Este   | 104 | 800000.00 | 735042.00
      13 | Atlanta     | Este   | 105 | 350000.00 | 367911.00
      21 | Los Angeles | Oeste  | 108 | 725000.00 | 835915.00
(5 rows)

```

```
## Postgres amb persistencia
docker run --rm --name training -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v postgres-data:/var/lib/postgresql/data --net 2hisx -d mporto20/postgres:prova 
--> Creacio de persistencia de dades

psql -h 172.18.0.2 -U postgres -d training -c "drop table oficinas;"
psql -h 172.18.0.2 -U postgres -d training -c "select * from oficinas;"
ERROR:  relation "oficinas" does not exist
LINE 1: select * from oficinas;

docker stop training 

docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v $(pwd)/training:/docker-entrypoint-initdb.d -v postgres-data:/var/lib/postgresql/data --net 2hisx  -d postgres

psql -h 172.18.0.2 -U postgres -d training -c "select * from oficinas;"
ERROR:  relation "oficinas" does not exist
LINE 1: select * from oficinas;
--> Verifiquem que la taula esborrada anteriorment encara ho és
```
