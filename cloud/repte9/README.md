# REPTE 9 -> Comptador de visites

Implementar l’exemple de la documentació del comptador de visites.

## Creació del programa *app.py*

```
import time
import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World from Docker! I have been seen {} times.\n'.format(count)
```

## Creació del fitxers *requirements.txt*

*El serveis que ha d'instal·lar.*

```
flask
redis
```

## Creació del *Dockerfile*

*Creem el Dockerfile del docker que contindrà el programa comptador i el servei web*

```
FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP app.py
ENV FLASK_RUN_HOST 0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
CMD ["flask", "run"]
```

> *RUN pip install -r requirements.txt -> Instal·la tot alló que necessita per treballar segons els serveis que hi han al document.*

> *0.0.0.0* -> Totes les interfícies.*

> *CMD ["flask", "run"]* -> Inicialitza el servei flask.*

## Creació del *docker-compose.yml*

```
version: '3'
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
    environment:
      FLASK_ENV: development
  redis:
    image: "redis:alpine"
```

> *alpine -> Una versió de Linux (és crea amb aquesta distribució per la seva poca ocupació d'espai.*

> *redis -> Un gestor de BD de funcionament clau:valor.*

## Encendre el docker-compose

Encendre'l amb detach.

```
$ docker compose up -d
```

## Comprovació del funcionament

![image1](/home/users/inf/hisx2/a190074lr/Pictures/Validacions/Validacio1.png)

> *Actualitzem la pàgina per veure si actualitza el comptador (F5).*

> ![image1](/home/users/inf/hisx2/a190074lr/Pictures/Validacions/Validacio2.png)


## Editem el missatge del programa *app.py

Editem el programa *app.py*.

```
$ vim app.py

"Hello World from Docker"
```

> *Comprovem al cerca en el navegador si s'ha actualitzat el missatge.*

> ![image1](/home/users/inf/hisx2/a190074lr/Pictures/Validacions/Validacio3.png)



