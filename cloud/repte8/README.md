# LDAPSERVER + PHPLDAPADMIN
## Docker compose

**Creació del docker compose**

```
version: "3"
services:
  ldap:
    image: mporto20/ldap22:base
    container_name: ldapserver
    hostname: ldapserver
    ports:
      - "389:389"
    volumes:
      - ldap-config:/etc/ldap/slapd.d
      - ldap-db:/var/lib/ldap
    networks:
      - mynet
  phpldapadmin:
    image: mporto20/ldap22:phpldapadmin
    container_name: phpldapadmin.edt.org
    hostname: phpldapadmin.edt.org
    ports:
      - "80:80"
    networks:
      - mynet
networks:
  mynet:
volumes:
  ldap-config:
  ldap-db:

```

Ordre per executar el nostre file:
```
docker compose -f ldap-php.yml up -d

```

Verifiquem:
$ docker volume ls
DRIVER    VOLUME NAME
local     1eb3401c507b033c10dd13d75cfaaebafa068c8ded3c4e9ab6333e7238e97bf2
local     4b7fe6fe11722af1afe570de9fdb7baf8a42ec65021d4055ba84f27d019e3124
local     8de40c91db8402381077d30f755fca8db6309b58890c0cf60ad9a4101fb03b6e
local     repte8_ldap-config
local     repte8_ldap-db

