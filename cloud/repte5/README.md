# Marc Porto
# M06-ASO 
# Curs 2022-23
# Ldapserver 2022

```
Creacio d'un servidor LDAP a partir d'entrypoint

```
**Passos a seguir:**


- canviar al Dockerfile el CMD per ENTRYPOINT

- executa l’script startup, però rep els arguments:

	- initdb → ho inicialitza tot de nou i fa el populate de edt.org

	- slapd → ho inicialitza tot però només engega el servidor, sense posar-hi dades

	- start / edtorg / res → engega el servidor utilitzant la persistència de dades de la bd i de la 	configuració. És a dir, engega el servei usant les dades ja existents

	- slapcat nº (0,1, res) → fa un slapcat de la base de dades indicada 

*Imatges fetes a partir d'un script*
```
Ordre per fer imatge a partir de 'initdb' com a arg:

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d mporto20/ldap_repte:5 initdb

```

```
Ordre per fer imatge a partir de 'slapd' com a arg:

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d mporto20/ldap22_repte:5 slapd

```
Ordre per fer imatge a partir de 'start|edtorg|res' com a arg:

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-db:/var/lib/ldap -d mporto20/ldap_repte:5 

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-db:/var/lib/ldap -d mporto20/ldap_repte:5 start

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-db:/var/lib/ldap -d mporto20/ldap_repte:5 edtorg



```

```
Ordre per fer imatge a partir de 'slapcat' com 1r arg i '0|1|res' com a 2n ar com a 2n arg:

Per tal que l'ordre s'executi caldrà iniciar la imatge interactivament!

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-db:/var/lib/ldap -it mporto20/ldap_repte:5 slapcat 

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-db:/var/lib/ldap -it mporto20/ldap_repte:5 slapcat 0

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-db:/var/lib/ldap -it mporto20/ldap_repte:5 slapcat 1

```
