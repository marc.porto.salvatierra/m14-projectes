#! /bin/bash
# @MArcporto
# Octubre 2022
# Script que configura un LDAPserver

arg=$1
if [ $# -lt 1 ];
then
	arg="null"
fi

case $arg in
	"initdb")
		# planxar tot
		rm -rf /etc/ldap/slapd.d/*
		rm -rf /var/lib/ldap/*
		slaptest -f slapd.conf -F /etc/ldap/slapd.d
		slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
		chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
		/usr/sbin/slapd -d0

		echo "has fet initdb"
		;;
	"slapd")
		rm -rf /etc/ldap/slapd.d/*
		rm -rf /var/lib/ldap/*
		slaptest -f slapd.conf -F /etc/ldap/slapd.d
		chown -R openldap.openldap /etc/ldap/slapd.d
		/usr/sbin/slapd -d0

		echo "has fet slapd"
		;;
	"start"|"edtorg"|"null")
		/usr/sbin/slapd -d0

		echo "has fet start|edtorg|res"
		;;	
	"slapcat")
		echo "has fet slapcat $2"
		if [ $# -lt 2 ];then
			slapcat	
		else
		slapcat -n$2
		fi
		;;
esac
exit 0
