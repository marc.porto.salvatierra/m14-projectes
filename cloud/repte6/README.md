# LDAPSERVER 2022
# creacio de servidor LDAP amb variables d'entorn

- Implementar un servidor LDAP on el nom de l’administrador de la base de dades de l’escola sigui captat a través de variables d’entorn.

```
Ordre per tal de crear variables d'entorn directament amb docker run:

$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -e ADMIN="marc" -e PASSWD="marc" -d mporto20/ldap22_repte:6

```

```
Implementacio a l'startup.sh per tal que canvii slapd.conf

sed -i "s/cn=Manager,dc=edt,dc=org/cn=$ADMIN,dc=edt,dc=org/" slapd.conf
sed -i "s/rootpw secret/rootpw $PASSWD/" slapd.conf
```
