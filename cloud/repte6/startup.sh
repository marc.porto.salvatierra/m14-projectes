#! /bin/bash
# creacio servidor LDAP

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
sed -i "s/cn=Manager,dc=edt,dc=org/cn=$ADMIN,dc=edt,dc=org/" slapd.conf
sed -i "s/rootpw secret/rootpw $PASSWD/" slapd.conf

slaptest -f slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d
/usr/sbin/slapd -d0

