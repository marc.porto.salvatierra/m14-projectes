Comandes

# Creacio de volums
docker volume create ldap-config
docker volume create ldap-data

# Engegar imatge amb persistencia
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d edtasixm06/ldap22:base

docker exec -it ldap.edt.org ldapsearch -x -LLL -h 172.17.0.2 -b 'dc=edt,dc=org' uid=pere dn

# Eliminar compte pere
docker exec -it ldap.edt.org ldapdelete -vx -h 172.17.0.2 -D 'cn=Manager,dc=edt,dc=org' -w secret 'uid=pere pou=usuaris,dc=edt,dc=org'
